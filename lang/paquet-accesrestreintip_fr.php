<?php

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(
	// A
	'accesrestreintip_description' => 'Ce plugin permet donner accès à des zones suivant l’adresse IP du visiteur.',
	'accesrestreintip_nom' => 'Accès (non) restreint par IP',
	'accesrestreintip_slogan' => 'Donner accès aux contenus restreints suivant l’IP',
);
