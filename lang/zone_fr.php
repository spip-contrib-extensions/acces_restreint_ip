<?php

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(
	'champ_ips_explication' => 'Liste d’adresses IP ou de plages d’IP séparées par des virgules. Exemple : 10.5.0.1-10.5.22.13, 10.6.134.132.',
	'champ_ips_label' => 'Adresses IP',
);
